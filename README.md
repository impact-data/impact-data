# IMPaCT-Data: Programa de Ciencia de Datos de la Infraestructura de Medicina de Precisión asociada a la Ciencia y la Tecnología

IMPaCT-Data es el programa de IMPaCT que persigue apoyar el desarrollo de un sistema común, interoperable e integrado, de recogida y análisis de datos clínicos y moleculares aportando para ello el conocimiento y los recursos disponibles en el Sistema Español de Ciencia y Tecnología. Este desarrollo permitirá dar respuesta a preguntas de investigación a partir de los diferentes sistemas de información clínica y molecular disponibles. Fundamentalmente, persigue que los investigadores puedan disponer de una perspectiva poblacional basada en datos individuales.

El objetivo de este repositorio es centralizar la información referente al desarrollo de los diferentes componentes que se desarrollaran durante el proyecto (2021-2023).

## Contacta con nosotros
Estamos interesados en sus opiniones, puede darnos feedback añadiendo un 'issue' en el repositorio de código correspondiente (o en este) o utilizar el enlace para contactar con nosotros que está en la [web del proyecto](https://impact-data.bsc.es/).

## Contribución
Se ha definido una guía de contribución separada de este archivo, se encuentra en el archivo [CONTRIBUTING.md](https://gitlab.bsc.es/impact-data/impact-data/-/blob/main/CONTRIBUTING.md).

## Guías y Recomendaciones
Los documentos incluyendo las guías y recomendaciones serán publicamente accesibles en la [web del proyecto](https://impact-data.bsc.es/outcomes/guidelines/).

## Componentes software
En esta sección se muestra la lista de los componentes software desarrollados para el proyecto.

| Repositorio | Nombre | Descripción | Organización responsable |
| ----------- | ------ | ----------- | :----------------------: | 
| [web](https://gitlab.bsc.es/impact-data/impd_website) | Web del proyecto | Web en la que se publican los detalles, los resultados, las noticias y los eventos relacionados con el proyecto | BSC-CNS |
| [impd-beacon_cbioportal](https://gitlab.bsc.es/impact-data/impd-beacon_cbioportal) | Beacon v2 - cBioPortal | Proyecto para implementar un Beacon v2 a través de datos de cBioPortal | BSC-CNS, IMIM, EGA-CRG |
| [impd-beacon_omopcdm](https://gitlab.bsc.es/impact-data/impd-beacon_omopcdm) | Beacon v2 - OMOP CDM | Proyecto para implementar un Beacon v2 encima de una instancia de OMOP CDM | BSC-CNS, IMIM,EGA-CRG |

## Documentación
En esta sección se muestra la lista de enlaces relacionados con la documentación.

| Nombre | Descripción | Enlace | Organización responsable |
| ------ | ----------- | ------ | :----------------------: |
|Implementación de referencia | Instrucciones para la instalación de los componentes incluidos en la imple,mentación de referencia, se publican en la plataforma [ReadTheDocs](https://impact-data-implementacion-de-refencia.readthedocs.io/es/latest/) | [impd-reference_implementation-docs](https://gitlab.bsc.es/impact-data/impd-reference-implementation-docs) |  BSC-CNS |
|        |             |        |                          |
|        |             |        |                          |
|        |             |        |                          |


##  Licencia
Este repositorio no contiene código ni conomiciento licenciable por lo que no tiene licencia. Cada uno de los distintos componentes que se desarrollen como resultado de este proyecto, tanto sean componentes software o conocimiento, tendrán su propia licencia en el repositorio correspondiente.

## Agradecimientos

_El proyecto IMPaCT-Data (Exp. IMP/00019) ha sido financiado por el Instituto de Salud Carlos III, co-financiado por el Fondo Europeo de Desarrollo Regional (FEDER, “Una manera de hacer Europa”)._

