# Guía para contribuir en IMPaCT-Data

Hay diversas opciones para contribuir con IMPaCT-Data:

- Envíanos tus comentarios
- Contribuye con código
- Documentación

## Envíanos tus comentarios! 

Estamos interesados en sus opiniones, puede darnos feedback añadiendo un `issue` en el repositorio de código correspondiente (o en este) o utilizar el enlace para contactar con nosotros que está en la [página web del proyecto](https://impact-data.bsc.es/).

## Contribuye con código

Para separar las contribuciones para los diferentes componentes de software, hay que crear repositorios sesparados en los que los programadores puedan contribuir.

Antes de hacer 'commit' de código núevo:
- Por favor, asegurarse de que se está respetando la licencia del componentes correspondiente
- Crear siempre una rama nueva para tu código basada en la rama "master"
- Si la contribución está relacionada con un 'issue' conocido, incluye una referencia al 'issue' en el comentario del 'commit'

Las guías para los desarrolladores están en el directorio ["guia_programadores"](https://gitlab.bsc.es/impact-data/impact-data/-/tree/main/guia_programadores), que incluye:
- [Crear un nuevo repositorio de código](https://gitlab.bsc.es/impact-data/impact-data/-/blob/main/guia_programadores/nuevo_repositorio.md)
- [Gestión del código fuente](https://gitlab.bsc.es/impact-data/impact-data/-/blob/main/guia_programadores/gestion_codigo.md)
- ...

## Cómo crear un issue

Para evitar reportes duplicados se recomienda buscar en la lista de 'issues' abierto antes de crear un 'issue' nuevo.

Al crear un issue se debe incluir la información siguiente:
- Si es un error, incluir los detalles para poder repoducirlo. Si es posible incluir la lista de pasos a seguir y algunas capturas de pantalla
- Si es una sugerencia de mejora o nueva funcionalidad, por favor incluir suficientes detalles para que otros puedan entenderlo

En este proyecto se han definido un conjunto de etiquetas para diferenciar los tipos:
1. `bug`: errores o resultados no deseados
2. `new feature`: nueva funcionalidad o mejoras en las existentes.
3. `style`: cambios que se refieren a la estética en los componentes que tienen interficie con el usuario
4. `question`": cuando se requiere ayuda

Se recomienda seleccionar una de estas 4 etiquetas para facilitar la gestión de estos asuntos. Además de estas etiqutas, hay 2  más para indicar situaciones especiales:
1. `urgent`: un problema crítico, de alta prioridad, que debe ser tratado lo antes posible
2. `nice-to-have`: una questión con poca prioridad
