# Cómo nombrar a los repositorios de código

Para homogeneizar los nombres de los repositorios de código para los distintos componentes, se han definido las siguientes normas:
* El nombre estará en minúsculas.
* Se utilizará el prefijo `impd-`.
* Si el nombre que le queremos dar al repositorio se compone de más de una palabra, se utilizará el carácter subrallado (`_`) para separar las palabras.
* Para los repositorios que estén relacionados con una tecnología concreta se podrá añadir un sufijo separado con un guión (`-`) para indicar la tecnología. Por ejemplo, las recetas para crear un contenedor podrían tener el sufijo `-recipe` o `-api` para los componentes que exponen la funcionalidad a través de una interface.

Si hay varios repositorios de un mismo componente, se procurará que el nombre empieze de la misma manera para facilitar la visión global de los distintos componentes.

# Contenido mínimo

* Todos los repositoris deben contener el archivo `README.md` en su raíz con una descripción del componente y la información necesaria para su uso y/o despliegue 
* Todos los repositorios que contienen **código fuente** deben contener el archivo `LICENSE.md` con el texto de la licencia seleccionada

# Licencia

En este proyecto nos hemos comprometido a generar componentes de codigo abierto (OSS) por lo que habrá que escoger una licencia OSS para los directorios que contengan el código fuente de los componentes software. 

Para los componentes que no se correspondan con código, se escogerá una licencia Creative Commons, en cualquier caso la licencia escogida debe ser de tipo [`Free Cultural Works`](https://creativecommons.org/share-your-work/public-domain/freeworks).

En el archivo `README.md` debe incluirse una sección con el título **"Licencia"** formateada con el mismo nivel nivel de título (#, ##, ###, ...) del resto de secciones del archivo justo antes de la sección de agradecimientos (ver siguiente sección). Esta sección debe incluir:
* el badge correspondiente a la licencia seleccionada utilizando https://shields.io/category/license, por ejemplo:
    * [![License: CC BY-SA 4.0](https://img.shields.io/badge/license-CC%20BY--SA%204.0-lightgrey)](https://creativecommons.org/licenses/by-sa/4.0/) (use lightgrey for CC licenses)
    * [![License: GPL-3.0](https://img.shields.io/badge/license-GPL--3.0-brightgreen)](https://www.gnu.org/licenses/gpl-3.0.en.html) (use brightgreen for OSS licenses)
* el nombre de la licencia, seguido de: "Ver [`LICENSE.md`](LICENSE.md)." que hace referencia al archivo LICENSE.md que se debe incluir en el repositorio

# Agradecimientos

En el archivo `README.md` debe incluirse una última sección con el título **"Agradecimientos"** formateada con el mismo nivel nivel de título (#, ##, ###, ...) del resto de secciones del archivo. Esta sección debe incluir la siguiente frase (en cursiva) mencionado la fuente de financiación del proyecto:

_El proyecto IMPaCT-Data (Exp. IMP/00019) ha sido financiado por el Instituto de Salud Carlos III, co-financiado por el Fondo Europeo de Desarrollo Regional (FEDER, “Una manera de hacer Europa”)._


