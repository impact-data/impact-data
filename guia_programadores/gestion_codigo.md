# Gestión del código

Para la gestión del código, los desarrolladores deben utilizar al menos las ramas `master` and `development` descritas en el [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) de [www.attassian.com](www.attassian.com) para diferneciar el código estable (última versión del componente) del código de la versión que está en desarrollo.

<img src="https://wac-cdn.atlassian.com/dam/jcr:cc0b526e-adb7-4d45-874e-9bcea9898b4a/04%20Hotfix%20branches.svg?cdnVersion=505" width="50%" alt="Gitflow Workflow">

# Commits
Para mejorar el seguimiento de los commits se distinguen entre los siguientes tipos:
- `fix`: cuando los cambios se han hecho para arreglar un error/bug
- `feat`: cuando los cambios se han hecho para incluir nueva funcionalidad o mejorar una existente
- `content`: cuando el cambio se refiere a añadir contendio, por ejemplo en una página web o en un archivo como éste
- `style`: cuando el cambio se refiere a la estética

Al hacer un commit, su comentario:
- debe emprezar con uno de los tipos anteriores ("fix", "feat" o "content") seguido de el carácter **":"**
- si está relacionado con un 'issue' concreto, incluir su número


